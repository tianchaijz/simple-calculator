.PHONY: all clean


mycalc: mycalc.l mycalc.y
	yacc -dv mycalc.y
	lex mycalc.l
	cc -o mycalc y.tab.c lex.yy.c

all: mycalc

clean:
	rm -f *.o mycalc
